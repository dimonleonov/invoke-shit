param (
    [string]$Computer = ($env:computername),
    [string]$User = "",    
    [string]$Command,
    [string]$Args
 )

$script_task = {

    param (
        [string]$User = "",
        [string]$Command,
        [string]$Args
     )

    #Action
    $Action = New-ScheduledTaskAction –Execute $Command
    if($Args.Length > 0) { $Action = New-ScheduledTaskAction –Execute $Command -Argument $Args}

    #Principal
    $P = New-ScheduledTaskPrincipal -UserId $User -LogonType Interactive -ErrorAction Ignore

    #Settings
    $S = New-ScheduledTaskSettingsSet -MultipleInstances Parallel -Hidden

    #Create TEMPTASK
    $TASK = New-ScheduledTask -Action $Action -Settings $S -Principal $P

    #Unregister old TEMPTASK
    Unregister-ScheduledTask -TaskName 'TEMPTASK' -ErrorAction Ignore -Confirm:$false

    #Register TEMPTASK
    Register-ScheduledTask -InputObject $TASK -TaskPath '\KD\' -TaskName 'TEMPTASK'

    #Execute TEMPTASK
    Get-ScheduledTask -TaskName 'TEMPTASK' -TaskPath '\KD\' | Start-ScheduledTask

    #Unregister TEMPTASK
    Unregister-ScheduledTask -TaskName 'TEMPTASK' -ErrorAction Ignore -Confirm:$false
}

#The scriptblock get the same parameters of the .ps1
Invoke-Command -ComputerName $Computer -ScriptBlock $script_task -ArgumentList $User, $Command, $Args
# EXAMPLE: file.ps1 -User USER_NAME -Command notepad.exe -Computer REMOTE_COMPUTER