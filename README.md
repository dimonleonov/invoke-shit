# invoke-shit
Данный powershell скрипт позволяет вам запустить exe-шник удаленно. Причем, что довольно важно - exe-шник запускается внутри сессии необходимого вам пользователя.
По сути, данный скрипт заменяет psexec (`PsExec.exe -i НОМЕР_СЕССИИ -d -s \\YOUR_COMPUTER "C:\blabla\soft.exe"`), если он по какой-то причине вам не подошел.

В данный момент данный скрипт использовался в рамках AD-домена, учетная запись от которой выполнялся скрипт имела права администратора для необходимой OU, в которой был целевой комп. 

# Что надо для использования софта
1) Админская уз, с которой вы сможете залогиниться на целевом компе
2) Разрешение на использование Invoke-Command для данной уз на целевом компе ([ВТФ](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/invoke-command?view=powershell-7.4))


# Логика работы
1) Создается скрытая служба в windows с нужным вам софтом
2) Софт запускается
3) Служба удаляется
4) ???
5) profit!

# Как использовать
Простой вариант:
1. `.\invoke-shit.ps1 -User TargetUser -Command notepad.exe -Computer Target_Computer`

Если нужно запустить какой-то другой exe:
1. Залейте нужный exe в целевой компьютер (проводник -> `\\Target_Computer\c$` -> копипастим нужный exe на целевой комп)
2. `.\invoke-shit.ps1 -User TargetUser -Command "C:\Path_On_Target_Computer\your_exe_file.exe" -Computer Target_Computer`

Target_Computer - целевой комп (например: `DESKTOP-123456`)
Target_User - целевой пользователь (например: `MyUser`)


# У МЕНЯ РУГАЕТСЯ НА ТРАСТЕД ХОСТС!!!
Если надо попроще: 
`Set-Item WSMan:localhost\client\trustedhosts -value *`
(удаленно: `Invoke-Command -ComputerName YourTargetComputer -ScriptBlock {Set-Item WSMan:localhost\client\trustedhosts -value *}` )
(несесурно, не рекомендую так делать)

Если надо посложнее:
`Set-Item WSMan:\localhost\Client\TrustedHosts host.yourdomain.local -Concatenate`
(удаленно: `Invoke-Command -ComputerName YourTargetComputer -ScriptBlock {Set-Item WSMan:\localhost\Client\TrustedHosts host.yourdomain.local -Concatenate}` )